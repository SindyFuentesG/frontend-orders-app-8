import React from "react";
import TextField from "@material-ui/core/TextField";
import { Formik, useFormik } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { Row } from "react-flexbox-grid";
import MenuItem from "@material-ui/core/MenuItem";
import { Button } from "@material-ui/core";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 300,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  },
}));

const estados = [
  {
    value: "PUBLICADO",
    label: "Publicado",
  },
  {
    value: "BORRADOR",
    label: "Borrador",
  },
];

const validationSchema = Yup.object().shape({
  nombre: Yup.string()
    .max(100, "Valor muy largo!")
    .required("Valor requerido!"),
  descripcion: Yup.string()
    .max(280, "Valor muy largo!")
    .required("Valor requerido!"),
  precioBase: Yup.number()
    .min(0, "Valor muy bajo!")
    .required("Valor requerido!"),
  tasaImpuestos: Yup.number()
    .min(0, "Valor muy bajo!")
    .max(1, "Valor muy grande!")
    .required("Valor requerido!"),
  estado: Yup.string().min(1, "Valor muy bajo!").required("Valor requerido!"),
  cantidadInventario: Yup.number()
    .min(0, "Valor muy bajo!")
    .required("Valor requerido!"),
});

const CreateProduct = () => {
  const classes = useStyles();
  const formik = useFormik({
    initialValues: {
      nombre: "",
      descripcion: "",
      precioBase: 0.0,
      tasaImpuestos: 0.0,
      estado: "",
      cantidadInventario: 0,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log(JSON.stringify({ values }, null, 2));
    },
  });
  return (
    <>
      <Row center="xs">
        <h2>Crear Producto</h2>
      </Row>

      <Row center="xs">
        <form className={classes.root} onSubmit={formik.handleSubmit}>
          <Row>
            <TextField
              id="nombre"
              name="nombre"
              label="Nombre"
              variant="outlined"
              value={formik.values.nombre}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.errors.nombre && formik.touched.nombre}
              helperText={
                formik.errors.nombre && formik.touched.nombre
                  ? formik.errors.nombre
                  : ""
              }
            />
            <TextField
              id="precioBase"
              name="precioBase"
              label="Precio Base"
              type="number"
              variant="outlined"
              value={formik.values.precioBase}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.errors.precioBase && formik.touched.precioBase}
              helperText={
                formik.errors.precioBase && formik.touched.precioBase
                  ? formik.errors.precioBase
                  : ""
              }
            />
          </Row>
          <Row>
            <TextField
              id="tasaImpuestos"
              name="tasaImpuestos"
              label="Tasa Impuestos"
              type="number"
              variant="outlined"
              value={formik.values.tasaImpuestos}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.errors.tasaImpuestos && formik.touched.tasaImpuestos
              }
              helperText={
                formik.errors.tasaImpuestos && formik.touched.tasaImpuestos
                  ? formik.errors.tasaImpuestos
                  : ""
              }
            />
            <TextField
              id="cantidadInventario"
              name="cantidadInventario"
              label="Cantidad Inventario"
              type="number"
              variant="outlined"
              value={formik.values.cantidadInventario}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={
                formik.errors.cantidadInventario &&
                formik.touched.cantidadInventario
              }
              helperText={
                formik.errors.cantidadInventario &&
                formik.touched.cantidadInventario
                  ? formik.errors.cantidadInventario
                  : ""
              }
            />
          </Row>
          <Row>
            <TextField
              id="descripcion"
              name="descripcion"
              multiline
              rows={4}
              rowsMax="6"
              label="Descripción"
              variant="outlined"
              value={formik.values.descripcion}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.errors.descripcion && formik.touched.descripcion}
              helperText={
                formik.errors.descripcion && formik.touched.descripcion
                  ? formik.errors.descripcion
                  : ""
              }
            />
            <TextField
              id="estado"
              name="estado"
              select
              label="Estado"
              variant="outlined"
              value={formik.values.estado}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.errors.estado && formik.touched.estado}
              helperText={
                formik.errors.estado && formik.touched.estado
                  ? formik.errors.estado
                  : ""
              }
            >
              {estados.map((estado) => (
                <MenuItem key={estado.value} value={estado.value}>
                  {estado.label}
                </MenuItem>
              ))}
            </TextField>
          </Row>
          <Row center="xs">
            <Button type="submit" variant="contained" color="primary">
              Crear Producto
            </Button>
          </Row>
        </form>
      </Row>
    </>
  );
};

export default CreateProduct;
