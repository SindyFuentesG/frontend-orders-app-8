import React, { useEffect, useState } from "react";
import { Row, Col } from "react-flexbox-grid";
import ProductListData from "product-services";
import ProductItem from "./item";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import { Fab } from "@material-ui/core";
import { Link } from "@reach/router";

const useStyles = makeStyles({
  cardItem: {
    marginTop: 10,
  },
  rowItem: {
    margin: 10,
  },
  addFab: {
    marginTop: 15,
  },
});

const useProducts = () => {
  const [productList, setProductList] = useState([]);
  const [error, setError] = useState(false);
  const [load, setLoad] = useState(false);

  useEffect(() => {
    ProductListData()
      .then((response) => {
        setProductList(response);
      })
      .catch((err) => {
        setError(true);
        console.log(err);
      })
      .finally(() => setLoad(true));
  }, [setProductList]);

  return {
    productList,
    load,
    error,
  };
};

const ProductList = () => {
  const classes = useStyles();
  const { productList, load, error } = useProducts();
  const productItems = productList.map((product) => {
    return (
      <Col xs={6} md={4} lg={3} key={product.id} className={classes.cardItem}>
        <ProductItem product={product} />
      </Col>
    );
  });

  if (!load) {
    return <h3>Cargando...</h3>;
  } else if (error) {
    return <h3>Se presentó un error al consultar el listado de productos</h3>;
  } else {
    return (
      <>
        <Row center="xs">
          <Col xs={10}>
            <h2>Lista de Productos</h2>
          </Col>
          <Col xs={2}>
            <Link to={"/create"}>
              <Fab
                size="small"
                color="primary"
                aria-label="add"
                className={classes.addFab}
              >
                <AddIcon />
              </Fab>
            </Link>
          </Col>
        </Row>
        <Row className={classes.rowItem}>{productItems}</Row>
      </>
    );
  }
};

export default ProductList;
