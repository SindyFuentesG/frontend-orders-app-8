import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

const useStyles = makeStyles({
  pos: {
    marginBottom: 12,
  },
});

const ProductItem = ({ product }) => {
  const classes = useStyles();
  const { nombre, descripcion, precioBase } = product;

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="h2">
          {nombre}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          $ {precioBase}
        </Typography>
        <Typography variant="body2" component="p">
          {descripcion}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">More Info</Button>
      </CardActions>
    </Card>
  );
};

ProductItem.propTypes = {
  product: PropTypes.shape({
    nombre: PropTypes.string.isRequired,
    descripcion: PropTypes.string.isRequired,
    precioBase: PropTypes.number.isRequired,
  }).isRequired,
};

export default ProductItem;
