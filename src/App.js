import React from "react";
import { Router } from "@reach/router";
import ListProducts from "./components/product-list";
import CreateProduct from "./components/create-product";

const Home = () => (
  <>
    <ListProducts />
  </>
);

function App() {
  return (
    <Router>
      <Home path="/" />
      <CreateProduct path="create" />
    </Router>
  );
}

export default App;
